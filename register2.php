<!DOCTYPE HTML>  
<html>
<head>
</head>
<body>  

<?php
// define variables and set to empty values
$nameErr = $emailErr = $bdate = $genderErr = $websiteErr = "";
$name = $email = $bdateErr = $gender = $comment = $website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } if(strlen($_POST["name"]) < 2  ) {
    $nameErr = "Name minimum 2 characters";
  }

  
    else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed"; 
    }
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
    }
    elseif(strlen(strstr($_POST["email"],'@', true)) < 6  ) {
        $emailErr = "Email min 6 characters";
      }
      elseif (checkEmailAdress($email)) {
        $emailErr = "Must be Gmail";
      }
  
    
  }
  
  if (empty($_POST["bdate"])) {
    $bdateErr = "Birth date is required";
  } else {
    $bdate = test_input($_POST["bdate"]);
  } 

  if (empty($_POST["website"])) {
    $website = "";
  } else {
    $website = test_input($_POST["website"]);
    // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
      $websiteErr = "Invalid URL"; 
    }
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }
  
  if (!empty($nameErr) or !empty($emailErr) or !empty($bdateErr) or !empty($websiteErr) or !empty($commentErr) or !empty($genderErr)) {
    $params = "name=" . urlencode($_POST["name"]);
    $params .= "&email=" . urlencode($_POST["email"]);
    $params .= "&bdate=" . urlencode($_POST["bdate"]);
    $params .= "&website=" . urlencode($_POST["website"]);
    $params .= "&comment=" . urlencode($_POST["comment"]);
    $params .= "&gender=" . urlencode($_POST["gender"]);

    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&emailErr=" . urlencode($emailErr);
    $params .= "&bdateErr=" . urlencode($bdateErr);
    $params .= "&websiteErr=" . urlencode($websiteErr);
    $params .= "&commentErr=" . urlencode($commentErr);
    $params .= "&genderErr=" . urlencode($genderErr);
    
    header("Location: index2.php?" . $params);
  }  else {
    echo "<h2>Your Input:</h2>";
    echo "Name: " . $_POST['name'];
    echo "<br>";
    
    echo "Email: " . $_POST['email'];
    echo "<br>";

    $date = date_create ($bdate);
    echo "Birth date: " .date_format($date,"F j, l, Y");
    echo "<br>";
    
    echo "Website: " . $_POST['website'];
    echo "<br>";
    
    echo "Comment: " . $_POST['comment'];
    echo "<br>";
    
    echo "Gender: " . $_POST['gender'];  
    echo "<br>";
    echo "<br>";
    
    echo "<a href=\"index2.php\">Return to form</a>";
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function checkEmailAdress($data) {
    if (!preg_match("/@gmail.com/",$data) === true)
    return true;
    else
    return false;
  }  
  
?>

</body>
</html>